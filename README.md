# TAREFA TERCEIRO TRIMESTRE

 `Tarefa 3o Trimestre` da asinatura de `DIW`. Pretendese dar covertura ós requisitos descritos no [enunciado de mestre][enunciado] da tarefa (tamén se pode ver unha copia en markdown [neste repo][enunciadoMD]).

 Pódese ver un exemplo de funcionamento en [https://diw.netlify.app/][sitioWeb]

[//]: # (Listado dos links empregados)

   <!-- Enlaces a MESTRE -->
   [enunciado]:
   <https://mestre.iessanclemente.net/mod/folder/view.php?id=31806>

   <!-- Enlaces a internos -->
   [enunciadoMD]:
   <doc/ENUNCIADO.md>

   <!-- Enlace sito web -->
   [sitioWeb]:
   <https://diw.netlify.app/>