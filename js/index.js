
"use strict";

// --------------------------------------------------------------------------------
// -- FUNCIÓN PRINCIPAL
// --------------------------------------------------------------------------------
function main() {
    // Listeners
    $(".copiar").on("click", copiarPortapapeis);
    $(".contraer").on("click", ocultarElementos);

    // Crear seccións de código
    $(".exemplo").each((i, item) => {
        // Copio o código do exemplo
        let html = formatearHTML($(item).html());

        // Xero o contido do bloque de código
        $(item).siblings("section").find(".codigo").text(html);
    });
}

// --------------------------------------------------------------------------------
// -- FUNCIÓNS
// --------------------------------------------------------------------------------
function formatearHTML(html) {
    let textos = html.split("\n")
    let result = ``;

    // Expresión regular para contar os espacios que hai ó comezo dun string
    // Poño un límite de carecteres moi alto para asegurarme de que se contan tódolos espacios
    const regExEspacios = /^\s{0,2000}/;

    let espaciosQuitar = contarEspaciosInicio(textos);

    textos.forEach((element) => {
        let espacios = element.match(regExEspacios)[0].length;
        let temp = element.split("");

        if (espacios > 0) {
            result += element.substring(espaciosQuitar);
        } else {
            result += element;
        }

        //result += restalarHTML(result) + "\n";
        result += "\n";
    });

    return result;
}

function restalarHTML(html) {
    const expRegTagIni = /<[a-z0-9\"\s\=\'\-]{0,}>/gi;
    const expRegTagFin = /<\/[a-z0-9]+>/gi;
    const expRegAttr = /\="[a-z0-9\"\s\=\'\-]{0,}/gi;

    const tagIni = [...html.matchAll(expRegTagIni)];
    const tagFin = [...html.matchAll(expRegTagFin)];

    let contido = $(html).text().trim();
    let result = ``;
    
    if (tagIni != null) {
        for(let item of tagIni) {
            let attr = [...item[0].matchAll(expRegAttr)];
            let attrStr = ``;
            let itemTemp = ``;

            if (attr.length > 0) {
                attrStr = `<code class="texto-verde">${attr[0]}</code>`;
                itemTemp = item[0].replace(attr[0], attrStr);
            }

            result += `<code class="texto-azul">${itemTemp}</code>`;
        }
    }
    
    if (contido != null) {
        //result += console;
    }
    
    if (tagFin != null) {
        for(let item of tagIni) {
            result += `<code class="texto-azul">${item}</code>`;
        }
    }

    return result;
}

// https://stackoverflow.com/questions/1229518/javascript-regex-replace-html-chars
function escaparChars(html) {
    const regExEtiquetas = /[&"'<>]/g;

    let result = html.replace(
        regExEtiquetas,
      (char) => ({
          "&"   : '&amp;',
          "\""  : '&quot;',
          "'"   : '&#39;',
          "<"   : '&lt;',
          ">"   : '&gt;',
        })[char]
    );

    return result;
}

/**
 * Busca o menor número de espacios cos que empezan un array de cadeas superior a 0.
 *
 * @param {string[]} arrayTextos
 * @returns número de espacios mínimo
 */
 function contarEspaciosInicio(arrayTextos) {
     let result = 2000;

     // Expresión regular para contar os espacios que hai ó comezo dun string
     // Poño un límite de carecteres moi alto para asegurarme de que se contan tódolos espacios
    const regExEspacios = /^\s{0,2000}/;

    arrayTextos.forEach((element) => {
        let espacios = element.match(regExEspacios)[0].length;

        if (espacios > 0 && espacios < result) {
            result = espacios;
        }
    });

    return result;
}

// --------------------------------------------------------------------------------
// -- LISTENERS
// --------------------------------------------------------------------------------
function copiarPortapapeis() {
      const code = $(this).next().children("code.codigo").text();
      navigator.clipboard.writeText(code);
}

function ocultarElementos() {
    $(this).siblings(".contraible").each((i, item)=>{
        $(item).toggle();
    });
}

// --------------------------------------------------------------------------------
// -- ALTERNATIVA A JQUERY
// --------------------------------------------------------------------------------
// https://stackoverflow.com/questions/9899372/pure-javascript-equivalent-of-jquerys-ready-how-to-call-a-function-when-t/9899701#9899701
function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

// --------------------------------------------------------------------------------
// -- INICIO DO PROGRAMA
// --------------------------------------------------------------------------------
// $(document).ready(()=>{
//     main();
// });

docReady(function() {
    main();
});
