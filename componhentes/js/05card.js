/** ####################################################################################### */
/** ## TARXETAS
/** ####################################################################################### */
// Axusta os anchos personalizados das tarxetas
function axustarAnchosTarexetas() {
    let tarxetas = document.querySelectorAll(".tarxeta");

    for (let i=0; i < tarxetas.length; i++) {
        let width = tarxetas[i].dataset.width;
        tarxetas[i].style.setProperty('--tarxeta-w', `${width}%`);
    }
}

docReady(function() {
    axustarAnchosTarexetas();
});
