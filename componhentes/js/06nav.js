/** ####################################################################################### */
/** ## MENÚ BURGUER
/** ####################################################################################### */
function menuBurger() {
    let navs = document.querySelectorAll("nav.menu-nav");

    for (let i = 0; i < navs.length; i++) {
        let menuCollapse = navs[i].querySelector(".menu-collapse");
        let menuItems = navs[i].querySelector(".menu-nav-items");

        menuCollapse.addEventListener("click", (event) => {
            menuItems.classList.toggle("menu-nav-visble");
        });
    }
}

docReady(function() {
    menuBurger();
});
