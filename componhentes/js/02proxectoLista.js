/** ####################################################################################### */
/** ## LISTA DE PROXECTOS
/** ####################################################################################### */
function hoverVideo() {
    let proxectoVideos = document.querySelectorAll(".pr-lista-video video");

    for (let i = 0; i < proxectoVideos.length; i++) {
        //// Listener para cambiar o efecto ó pasar o rato por enriba
        proxectoVideos[0].addEventListener("mouseover", (event) => {
            event.target.play();
        }, false);

        //// Listener para cambiar o efecto ó sacar o rato de enriba
        proxectoVideos[0].addEventListener("mouseleave", (event) => {
            event.target.load();
        }, false);
    }
}

docReady(function() {
    hoverVideo();
});
