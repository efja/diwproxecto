/** ####################################################################################### */
/** ## DETALLES DE PROXECTOS
/** ####################################################################################### */
function proxectoItem() {
    let proxectos = document.querySelectorAll(".proxecto-container");

    for (let i = 0; i < proxectos.length; i++) {
        //// Listener para abrir a modal
        let idProxectoItem = proxectos[i].id;
        let proxectoItem = document.querySelector(`[data-idProxecto='#${idProxectoItem}']`);

        proxectoItem.addEventListener("click", (event) => {
            proxectos[i].style.display = "flex";
        });

        //// Listener para ocultar a modal
        let btnPecharProxecto = document.querySelector(`#${idProxectoItem} .btn-pechar`);

        btnPecharProxecto.addEventListener("click", (event) => {
            proxectos[i].style.display = "none";

            // Para parar os videos en reproducción ó pechar o proxecto
            // Só debería haber un único video pero por se acaso se pon algún máis no corpo
            let videos = proxectos[i].querySelectorAll("video");
            for (let i = 0; i < videos.length; i++) {
                videos[i].load();
            }
        });
    }
}

function stopVideo() {
    let proxectoVideos = document.querySelectorAll(".proxecto-video");

    for (let i = 0; i < proxectoVideos.length; i++) {
        let proxectoVideo = proxectoVideos[0].querySelector("video");
        let icoVideo = proxectoVideos[0].querySelector(".pr-play");

        //// Listener para cambiar o efecto ó reproducirse
        proxectoVideo.addEventListener("playing", (event) => {
            event.target.play();
            icoVideo.style.display = "none";
        }, false);

        //// Listener para cambiar o efecto ó parar
        proxectoVideo.addEventListener("pause", (event) => {
            icoVideo.style.display = "inline-block";
        }, false);

        //// Listener para cambiar o efecto ó parar
        proxectoVideo.addEventListener("ended", (event) => {
            event.target.load();
            icoVideo.style.display = "inline-block";
        }, false);
    }
}

docReady(function() {
    proxectoItem();
    stopVideo();
});
