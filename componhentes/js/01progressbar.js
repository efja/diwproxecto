/** ####################################################################################### */
/** ## BARRAS DE PROGRESO
/** ####################################################################################### */
function encherProgressbar() {
    let pgbars = document.querySelectorAll(".progressbar .progressbar-fill");

    for (let i=0; i < pgbars.length; i++) {
        let pgFill = pgbars[i].dataset.progress;
        let pgCorBase = pgbars[i].dataset.pgCorBase;
        let pgCorAlt = pgbars[i].dataset.pgCorAlt;

        pgbars[i].style.setProperty('--pg-w-fill', `${pgFill}%`);
        if (pgCorBase != undefined && pgCorBase != null) {
            pgbars[i].style.setProperty('--pg-cor-animate-cor-base', `var(--cor-${pgCorBase})`);
        }
        if (pgCorAlt != undefined && pgCorAlt != null) {
            pgbars[i].style.setProperty('--pg-cor-animate-cor-alt', `var(--cor-${pgCorAlt})`);
        }
    }
}

docReady(function() {
    encherProgressbar();
});
