/** ####################################################################################### */
/** ## SPINNER
/** ####################################################################################### */
function spinner() {
    let spinnerDemoOK = document.querySelector("#spinnerDemoOK");

    spinnerDemoOK.addEventListener("animationend", (event) => {
        event.target.style.display = "none";
        spinnerDemoOK.classList.remove("spinner-out");
    });

    spinnerDemoOK.classList.add("spinner-out");
}

docReady(function() {
    spinner();
});
