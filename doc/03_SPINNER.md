# SPINNER

## Fondo

É requisito imprescidible que teña un fondo que oculte a carga da web, ainda que pode ter algo de transparencia.

## Funcionamento

Proponse o seguinte:
Usando o icono spinner de Font Awesome que deberá rotar mediante unha animación css.

## Exemplo

Non ten que ser igual a este, nin en tamaño nin en cor, este é un exemplo de cómo podería verse.

![spinner]

## Animación

O spinner ocultarase utilizando unha animación a criterio de cada alumno cando a páxina esté cargada utilizando unha animación. Unha vez a animación remate debemos aplicar a propiedade `display: none` para que o usuario poida interactuar co contido


[//]: # (Listado dos links empregados)

   <!-- IMAXES -->
   [spinner]:
   <img/spinner.png>
