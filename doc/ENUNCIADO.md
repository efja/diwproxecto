# TAREFA TERCEIRO TRIMESTRE

Esta é unha copia do [enunciado][enunciado] da `Tarefa 3o Trimestre` da asinatura de `DIW` publicado en na plataforma [menstre][enunciado] do `IES San Clemente`.

## Descripción

A tarefa irémola definindo a medida que avanzamos coa materia para ir desenvolvendo parte por parte. En xeral consistirá en deseñar un proxecto web para o cal en primeiro lugar deseñaremos diferentes compoñentes que posteriormente implementaremos no proxecto.

## Compoñentes a deseñar

Podemos tomar como exemplos [bootstrap][bootstrap] ou [material design][materialDesign] para tomar ideas, pero **non** debemos facer un deseño idéntico.

- Deseño de progress bar. ***Engadido documento con detalles ([md][progressBarMD] - [GDrive][progressBarGDrive])**
- Deseño de proxectos: Listado de proxectos e vista de detalle do proxecto. **Ver detalles ([md][proxectosMD] - [GDrive][proxectosGDrive])**
- Deseño de spinner de carga. **Ver detalles ([md][spinnerMD] - [GDrive][spinnerGDrive])**  *Engadido requisito de animación no documento
- Deseño de cards. **Ver detalles ([md][cardsMD] - [GDrive][cardsGDrive])**
- Header e menú de navegación
- Footer (peche da web)
- Formulario de contacto
- ...

## Outros requisitos

- Deseño adaptable
- Uso de imaxes
- Uso de vídeo
- Uso de animacións
- Iconos [Font Awesome][fontAwesome]
- Accesibilidade: Ferramentas test [accesibilidade][accesibilidade]
- Usabilidade
- ...

## Outras consideracións

Cada día iremos engadindo compoñentes e definindo os requisitos do proxecto ademáis de enlazar descripcións ou recursos a utilizar.

**Recorda por comentarios explicando a estructura ou cousas a ter en conta se alguén reutiliza o teu código.**

## Definición do proxecto final

Proponse o deseño de un `CV`, pero o alumno pode propor calquera outra alternativa sempre e cando cumpla tódolos requisitos propostos.

O proxecto contará con un menú de navegación e **non** poderá ser deseño tipo OnePage (todo o contido en só unha páxina).

[//]: # (Listado dos links empregados)

   <!-- Enlaces a terceiros -->
   [bootstrap]:
   <https://getbootstrap.com/docs/5.0/getting-started/introduction/>

   [materialDesign]:
   <https://material.io/components>

   [fontAwesome]:
   <https://fontawesome.com/>

   [accesibilidade]:
   <https://www.w3.org/WAI/ER/tools/>

   <!-- Enlaces a MESTRE -->
   [enunciado]:
   <https://mestre.iessanclemente.net/mod/folder/view.php?id=31806>

   <!-- Enlaces a Google Drive -->
   [progressBarGDrive]:
   <https://>

   [proxectosGDrive]:
   <https://>

   [spinnerGDrive]:
   <https://>

   [cardsGDrive]:
   <https://>

   <!-- Enlaces a internos -->
   [progressBarMD]:
   <01_PROGRESBAR.md>

   [proxectosMD]:
   <02_PROXECTOS.md>

   [spinnerMD]:
   <03_SPINNER.md>

   [cardsMD]:
   <04_CARDS.md>
