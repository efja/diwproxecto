# PROXECTOS

## Compoñente proxecto:

Uso de Fontawesome

## Lista de proxectos

- Imaxe*
- Imaxe alternativa con hover
- Título

## Detalle de proxecto

- Imaxe principal*
- Título
- Descripción
- Link: un ou varios, por exemplo a github (ou similar) onde o código fonte e ao proxecto en funcionamento acompañados de iconos de font awesome

> \* O compoñente debe permitir o uso de vídeo en lugar de imaxes. No listado amosaráse unha imaxe igualmente co icono de vídeo superposto mentres que cando entramos ao detalle do proxecto amosarase o vídeo, pero utilizando as propiedades html e css debemos amosar unha portada propia. O vídeo será un ficheiro propio e non poderá ser usado youtube ou similares.
