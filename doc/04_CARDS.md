# CARDS

## Requisitos

- Imaxe opcional (quero decir que pode levar ou non imaxe dependendo dos contidos a montar pero debe soportar ambas opcións)
- Título
- Descripción
- Botóns

## Exemplos

É recomendable deseñar un container para este tipo de elementos co fin de adaptar o número de cards por fila que se amosarán en función do tamaño de pantalla.

![cards1]

![cards2]

[//]: # (Listado dos links empregados)

   <!-- IMAXES -->
   [cards1]:
   <img/cards1.png>

   [cards2]:
   <img/cards2.png>
