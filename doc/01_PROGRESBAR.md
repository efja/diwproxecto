# PROGRESBAR

## Deseño

O deseño do elemento é libre, pero debe conter:

- Un título opcional (con opcional refírome a que pode existir en uns casos e en outros non) para por exemplo indicar o skill ou habilidade ao que se refire
- Fondo: Este permitirá apreciar cal sería o 100% da barra
- Frente: Este será o elemento que indique a candidade porcentual acadada pola barra de progreso.

## Animación

O elemento de frente deberá agrandarse con unha animación ou transición ata acadar o tamaño final unha sóa vez.

## Asignación de tamaño

Utilizaremos Java Script para indicar o tamaño do elemento de frente con un atributo html similar a o seguinte exemplo (a ubicación do atributo queda a elección de cada alumno):

```html
<div class=”progress” data-progress=”75”>
…
</div>
```

## Exemplos

![progressbar1]

![progressbar2]

[//]: # (Listado dos links empregados)

   <!-- IMAXES -->
   [progressbar1]:
   <img/progressbar1.png>

   [progressbar2]:
   <img/progressbar2.png>
